use colored::*;
use std::fs;
use std::path::Path;
use std::time::SystemTime;

pub fn getsystemage() -> Result<(), Box<dyn std::error::Error>> {
    let root_dir = Path::new("/");
    let metadata = fs::metadata(root_dir)?;
    let created = metadata.created()?;

    let now = SystemTime::now();

    let duration = now.duration_since(created)?;
    let days = duration.as_secs() / (60 * 60 * 24);
    let suffix = if days == 1 { "Day" } else { "Days" };

    println!(
        "{}{} {} {}",
        crate::ASCII[6].blue(),
        "System age:".bold().blue(),
        days,
        suffix
    );

    Ok(())
}
