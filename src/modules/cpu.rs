use colored::*;
use libmacchina::{traits::GeneralReadout as _, GeneralReadout};

use std::path::Path;

pub fn getcpu() {
    if Path::new("C:/Windows").exists() {
        #[cfg(windows)]
        {
            use sysinfo::System;
            let mut sys = System::new_all();
            let cpu = sys.cpus().len();
            print!(
                "{}{} {}\n",
                crate::ASCII[6].blue(),
                "CPU:".bold().blue(),
                cpu
            );
        }
    } else {
        let general_readout = GeneralReadout::new();
        let cpu_cores = general_readout.cpu_cores().unwrap();
        let cpu = general_readout.cpu_model_name().unwrap();

        print!(
            "{}{} {} ({})\n",
            crate::ASCII[6].blue(),
            "CPU:".bold().blue(),
            cpu,
            cpu_cores
        );
    }
}
