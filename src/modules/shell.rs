use colored::*;
use regex::Regex;
use std::env;
use std::path::Path;
use std::process::Command;

fn run_cmd_unsafe(cmd: &str) -> String {
    let output = Command::new("sh")
        .args(["-c", cmd])
        .output()
        .unwrap()
        .stdout;
    String::from_utf8(output).unwrap().trim().to_string()
}

pub fn getshell() {
    if Path::new("C:/Windows").exists() {
        print!("{} PowerShell\n", "Shell:".bold().blue());
        return;
    }

    let ver_regex = Regex::new(r"(0|[1-9]\d*)\.(0|[1-9]\d*)\.?(0|[1-9]\d*)?(?:-((?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\+([0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?").unwrap();
    let find_shell = env::var("SHELL").unwrap();

    if Path::new(&format!("/usr/local/bin")).exists()
        && Path::new("/bin/freebsd-version").exists()
    {
        let shell = find_shell.replace("/usr/local/bin/", "");

        let version = run_cmd_unsafe(format!("{shell} --version").as_str());
        let locations = ver_regex.find(&version).unwrap();
        let version = &version[locations.start()..locations.end()];
        print!(
            "{}{} {shell} {version}\n",
            crate::ASCII[4].blue(),
            "Shell:".bold().blue()
        );
        return;
    }

    let mut shell_paths = find_shell.split('/').rev();

    if let Some(shell_name) = shell_paths.next() {
        if shell_name == "dash" {
            print!(
                "{}{} Dash\n",
                crate::ASCII[4].blue(),
                "Shell:".bold().blue()
            );

            return;
        }
        
        let is_nixos = Path::new("/run/current-system/sw").exists();
        let is_usr_bin = Path::new("/usr/bin").exists();

        let prefix = if is_nixos {
            "/run/current-system/sw/bin"
        } else if is_usr_bin {
            "/usr/bin"
        } else {
            "/bin"
        };

        let possible_filename = format!("{prefix}/{shell_name}");

        if !Path::new(&possible_filename).exists() {
            print!(
                "{}{} {shell_name}\n",
                crate::ASCII[4].blue(),
                "Shell:".bold().blue()
            );

            return;
        }

        // possible_filename exists so we can just fetch the version for
        // a better output!
        let version = run_cmd_unsafe(format!("{shell_name} --version").as_str());
        let locations = ver_regex.find(&version).unwrap();
        let version = &version[locations.start() .. locations.end()];

        print!(
            "{}{} {shell_name} {version}\n",
            crate::ASCII[4].blue(),
            "Shell:".bold().blue()
        );

        return;
    }

    // means `find_shell` is empty so no "$SHELL" available.
    print!(
        "{}{} Unknown\n",
        crate::ASCII[4].blue(),
        "Shell:".bold().blue()
    );
}
