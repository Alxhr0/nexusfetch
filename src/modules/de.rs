use colored::*;
use libmacchina::{traits::GeneralReadout as _, GeneralReadout};
use regex::Regex;
use std::process::Command;

fn run_cmd_unsafe(cmd: &str) -> String {
    let output = Command::new("sh")
        .args(["-c", cmd])
        .output()
        .unwrap()
        .stdout;
    String::from_utf8(output).unwrap().trim().to_string() 
}

pub fn getde() {
    let general_readout = GeneralReadout::new();

    // Use libmacchina's desktop_environment detection mechanism
    let de = general_readout
        .desktop_environment()
        .unwrap_or("unknown".into());

    // If de is unknown, hide the module.
    if de == "unknown" {
        return;
    }

    let ver_regex = Regex::new(r"(0|[1-9]\d*)\.(0|[1-9]\d*)\.?(0|[1-9]\d*)?(?:-((?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\+([0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?").unwrap();

    match de.as_str() {
        "Hyprland" => return,
        "Sway" => return,
        "river" => return,
        "Awesome" => return,
        "i3" => return,
        "DWM" => return,
        "KDE" => {
            let version = run_cmd_unsafe("plasmashell --version");

            if version.is_empty() {
                print!(
                    "{}{} {}\n",
                    crate::ASCII[6].blue(),
                    "DE:".bold().blue(),
                    "KDE Plasma"
                )
            } else {
                let locations = ver_regex.find(&version).unwrap();
                let version = &version[locations.start()..locations.end()];

                print!(
                    "{}{} {} {}\n",
                    crate::ASCII[6].blue(),
                    "DE:".bold().blue(),
                    "KDE Plasma",
                    version
                )
            }
        }
        "X-Cinnamon" => {
            let version = run_cmd_unsafe("cinnamon --version");
            let locations = ver_regex.find(&version).unwrap();
            let version = &version[locations.start()..locations.end()];

            print!(
                "{}{} {} {}\n",
                crate::ASCII[6].blue(),
                "DE:".bold().blue(),
                "Cinnamon",
                version
            )
        }
        "GNOME" => {
            let version = run_cmd_unsafe("gnome-shell --version");
            let locations = ver_regex.find(&version).unwrap();
            let version = &version[locations.start()..locations.end()];

            print!(
                "{}{} {} {}\n",
                crate::ASCII[6].blue(),
                "DE:".bold().blue(),
                "GNOME",
                version
            )
        }
        _ => {
            let version = String::from(if de.as_str() == "XFCE" {
                run_cmd_unsafe("xfce4-session --version").to_string()
            } else {
                return;
            });

            let locations = ver_regex.find(&version).unwrap();
            let version = &version[locations.start()..locations.end()];

            print!(
                "{}{} {} {}\n",
                crate::ASCII[6].blue(),
                "DE:".bold().blue(),
                de,
                version
            )
        }
    }
}
