use colored::Colorize;

#[cfg(feature = "with-selinux")]
pub fn getselinux() {
    use selinux::*;
    let is_selinux_supported = selinux::kernel_support();

    if let KernelSupport::SELinux | KernelSupport::SELinuxMLS = is_selinux_supported {
        let current_status = current_mode();
        println!(
            "{}{}: {:?}",
            crate::ASCII[6].blue(),
            "SELinux".bold().blue(),
            current_status
        );
    }
}
