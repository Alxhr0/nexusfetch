use colored::*;
use libmacchina::{traits::GeneralReadout as _, GeneralReadout};
use pciid_parser::Database;
use std::path::Path;

pub fn getgpu() {
    // Check for NixOS here and disable the module, because searching for the pci.ids file fails because of pci-id-parser supporting only 3 hard-coded paths.
    if Path::new("/run/current-system/sw").exists() {
        return;
    }

    // Disable the module if reading the pci.ids file failed.
    if Database::read().is_err() {
        return;
    }

    let general_readout = GeneralReadout::new();
    let gpu = general_readout.gpus();

    // Disable the module if gpu readout is invalid.
    if gpu.is_err() {
        return;
    }

    // it is safe now
    let gpu = gpu.unwrap();

    print!(
        "{}{} {}\n",
        crate::ASCII[6].blue(),
        "GPU:".bold().blue(),
        gpu.join(&format!(
            "\n{}{} ",
            crate::ASCII[6].blue(),
            "GPU2:".bold().blue()
        ))
    );
}
