use colored::*;
use libmacchina::{traits::GeneralReadout as _, GeneralReadout};

pub fn getwm() {

    /* 
        There is a bug which is happening on NixOS and probably on GUIX too. 
        Not really sure, but basically WM detection fails, when run in a FHS environment it works fine. 
        More investigation is needed.
    */

    let general_readout = GeneralReadout::new();

    let wm = general_readout.window_manager().unwrap_or("unknown".into());

    if wm == "unknown" {
        return;
    }

    print!("{}{} {}\n", crate::ASCII[6].blue(), "WM:".bold().blue(), wm);
}
