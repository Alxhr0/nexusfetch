use colored::*;
use dotenvy;
use regex::Regex;
use std::path::Path;
use clap::*;

pub fn getos() {
    let args = crate::cli::Cli::parse();

    if let Some(s) = args.os {
        print_os_info("OS", &s);
        return;
    }

    if Path::new("C:/Windows").exists() {
        #[cfg(windows)]
        {
            print_os_info("OS:", &detect_windows_version());
        }
    } else if is_bedrock_environment() {
        match read_os_release("/bedrock/etc/os-release") {
            Some(os_name) => print_os_info("OS:", &os_name),
            None => eprintln!("{}", "Failed to read OS release information.".red()),
        }
    } else if Path::new("/etc/os-release").exists() {
        match read_os_release("/etc/os-release") {
            Some(os_name) => print_os_info("OS:", &os_name),
            None => eprintln!("{}", "Failed to read OS release information.".red()),
        }
    }
}

fn print_os_info(label: &str, info: &str) {
    println!("{}{} {}", crate::ASCII[1].blue(), label.bold().blue(), info);
}

#[cfg(windows)]
fn detect_windows_version() -> String {
    use winver::WindowsVersion;
    WindowsVersion::detect().unwrap_or_else(|_| "Unknown Windows version".to_string())
}

fn is_bedrock_environment() -> bool {
    dotenvy::var("PATH")
        .map_or(false, |path| path.contains("/bedrock/cross/bin"))
}

fn read_os_release(path: &str) -> Option<String> {
    let content = std::fs::read_to_string(path).ok()?;
    let re = Regex::new(r#"(?m)^PRETTY_NAME=(?:(?:"(.*?)")|(?:(.*)))$"#).ok()?;
    let captures = re.captures(&content)?;

    captures.get(2).or(captures.get(1)).map(|m| m.as_str().to_string())
}
