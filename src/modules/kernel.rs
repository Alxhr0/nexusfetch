use colored::*;
use std::path::Path;
use libmacchina::{traits::KernelReadout as _, KernelReadout};

pub fn getkernel() {
    if Path::new("C:/Windows").exists() {
        #[cfg(windows)]
        {
            use winver::WindowsVersion;
            let version = WindowsVersion::detect().unwrap();
            print!(
                "{}{} {}\n",
                crate::ASCII[3],
                "Kernel: NT ".bold().blue(),
                version
            );
        }
    } else {
        let kernel_readout = KernelReadout::new();
        let kernel = kernel_readout.pretty_kernel().unwrap_or("Unknown kernel!".to_string());

        print!(
            "{}{} {}\n",
            crate::ASCII[3].blue(),
            "Kernel:".bold().blue(),
            kernel
        );
    }
}
