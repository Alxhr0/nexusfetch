use colored::*;
use libmacchina::{traits::GeneralReadout as _, GeneralReadout};
use std::path::Path;

pub fn gethost() {
    if Path::new("C:/Windows").exists() {
        return;
    }

    let general_readout = GeneralReadout::new();
    let host = general_readout.machine();

    if host.is_err() {
        return;
    }

    print!(
        "{}{} {}\n",
        crate::ASCII[2].blue(),
        "Host:".bold().blue(),
        host.unwrap()
    );
}
