use colored::*;
use libmacchina::{traits::GeneralReadout as _, GeneralReadout};
use rpm_pkg_count::count;
use std::{path::Path, process::Command};

fn getpkgs(file_path: &str, count_command: &str, package_manager_name: &str) {
    if Path::new(file_path).exists() {
        let mut command = Command::new("bash");
        command.arg("-c").arg(count_command);

        let pkgs_count = String::from_utf8(command.output().unwrap().stdout).unwrap();

        if pkgs_count.trim() != "0" {
            print!(" {} ({})", pkgs_count.trim(), package_manager_name);
        }
    }
}

pub unsafe fn getpkg() {
    print!("{}{}", crate::ASCII[6].blue(), "Pkgs:".bold().blue());

    let general_readout = GeneralReadout::new();
    let user = general_readout.username().unwrap();

    if Path::new("/bedrock/cross/bin").exists() {
        getpkgs("/bedrock/cross/bin/apk", "apk info | wc -l", "apk");
        getpkgs(
            "/bedrock/cross/bin/pacman",
            "pacman -Qs | wc -l | awk '{print $1/2}'",
            "pacman",
        );
        getpkgs(
            "/bedrock/cross/bin/apt",
            "dpkg --get-selections | wc -l",
            "dpkg",
        );
        getpkgs("/bedrock/cross/bin/emerge", "qlist -I | wc -l", "emerge");

        if Path::new("/bedrock/cross/bin/zypper").exists() {
            let package_manager_name = "zypper";
            match unsafe { count() } {
                Some(count) => print!(" {} ({})", count, package_manager_name),
                None => return,
            }
        }

        getpkgs(
            "/bedrock/cross/bin/xbps-install",
            "xbps-query -l | wc -l",
            "xbps",
        );
        getpkgs(
            "/bedrock/cross/bin/scratch",
            "scratch installed | wc -l",
            "scratch",
        );
        getpkgs(
            "/bedrock/cross/bin/slackpkg",
            "ls /var/log/packages | wc -l",
            "slackpkg",
        );

        getpkgs(
            "/bedrock/cross/bin/cave",
            "cave show */*::/ | wc -l | awk '{print $1 - 1}'",
            "cave",
        );

        if Path::new("/bedrock/cross/bin/dnf").exists() {
            let package_manager_name = "dnf";
            match unsafe { count() } {
                Some(count) => print!(" {} ({})", count, package_manager_name),
                None => return,
            }
        }
        getpkgs(
            "/bedrock/cross/bin/flatpak",
            "flatpak list | wc -l",
            "flatpak",
        );

        getpkgs(
            "/bedrock/cross/bin/snap",
            "snap list | wc -l | awk '{print $1 - 1}'",
            "snap",
        );

        getpkgs(
            "/bedrock/cross/bin/flatpak",
            "flatpak list | wc -l",
            "flatpak",
        );

        if Path::new("/run/current-system/sw/bin/").exists() {
            getpkgs(
                "/run/current-system/sw/bin/nix-store",
                "nix-store -qR /run/current-system/sw | wc -l",
                "nix-system",
            );
            getpkgs(
                "/run/current-system/sw/bin/nix-store",
                &format!("nix-store -qR /home/{}/.nix-profile | wc -l", user),
                "nix-user",
            );
            getpkgs(
                "/run/current-system/sw/bin/nix-store",
                &format!("nix-store -qR /etc/profiles/per-user/{} | wc -l", user),
                "nix-user",
            );
            getpkgs(
                "/run/current-system/sw/bin/flatpak",
                "flatpak list | wc -l",
                "flatpak",
            );
        } else {
            getpkgs(
                "/nix/var/nix/profiles/default/bin/nix-store",
                &format!("nix-store -qR /home/{}/.nix-profile | wc -l", user),
                "nix-user",
            );
            getpkgs(
                "/nix/var/nix/profiles/default/bin/nix-store",
                &format!("nix-store -qR /etc/profiles/per-user/{} | wc -l", user),
                "nix-user",
            );
        }
    } else {
        getpkgs("/sbin/apk", "apk info | wc -l", "apk");
        getpkgs(
            "/bin/cave",
            "cave show */*::/ | wc -l | awk '{print $1 - 1}'",
            "cave",
        );
        getpkgs(
            "/usr/bin/pacman",
            "ls --ignore ALPM_DB_VERSION /var/lib/pacman/local/ | wc -l",
            "pacman",
        );
        getpkgs("/usr/bin/apt", "dpkg --get-selections | wc -l", "dpkg");
        getpkgs("/usr/bin/emerge", "qlist -I | wc -l", "emerge");
        if Path::new("/usr/bin/zypper").exists() {
            let package_manager_name = "zypper";
            match unsafe { count() } {
                Some(count) => print!(" {} ({})", count, package_manager_name),
                None => return,
            }
        }
        getpkgs("/usr/bin/xbps-install", "xbps-query -l | wc -l", "xbps");
        getpkgs(
            "/usr/bin/scratch",
            "ls /var/lib/scratchpkg/db/ | wc -l",
            "scratch",
        );
        getpkgs(
            "/usr/sbin/slackpkg",
            "ls /var/log/packages/ | wc -l",
            "slackpkg",
        );

        getpkgs("/usr/sbin/pkg", "pkg info | wc -l", "pkg");

        if Path::new("/usr/bin/dnf").exists() && !Path::new("/usr/bin/rpm-ostree").exists() {
            let package_manager_name = "dnf";
            match unsafe { count() } {
                Some(count) => print!(" {} ({})", count, package_manager_name),
                None => return,
            }
        }

        if Path::new("/usr/bin/rpm-ostree").exists() {
            let package_maanger_name = "rpm-ostree";
            match unsafe { count() } {
                Some(count) => print!(" {} ({})", count, package_maanger_name),
                None => return,
            }
        }

        getpkgs(
            "/usr/bin/snap",
            "snap list | wc -l | awk '{print $1 - 1}'",
            "snap",
        );
        getpkgs("/usr/bin/flatpak", "flatpak list | wc -l", "flatpak");
        getpkgs("/home/linuxbrew/.linuxbrew/bin/brew", "brew list | wc -l", "brew");

        if Path::new("/run/current-system/sw/bin/").exists() {
            getpkgs(
                "/run/current-system/sw/bin/nix-store",
                "nix-store -qR /run/current-system/sw | wc -l",
                "nix-system",
            );
            getpkgs(
                "/run/current-system/sw/bin/nix-store",
                &format!("nix-store -qR /etc/profiles/per-user/{} | wc -l", user),
                "nix-user",
            );
            getpkgs(
                "/run/current-system/sw/bin/flatpak",
                "flatpak list | wc -l",
                "flatpak",
            );
        } else {
            getpkgs(
                "/nix/var/nix/profiles/default/bin/nix-store",
                &format!("nix-store -qR /home/{}/.nix-profile | wc -l", user),
                "nix-user",
            );
            getpkgs(
                "/nix/var/nix/profiles/default/bin/nix-store",
                &format!("nix-store -qR /etc/profiles/per-user/{} | wc -l", user),
                "nix-user",
            );
        }
    }
}
