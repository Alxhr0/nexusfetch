mod modules;

mod cli;
use clap::Parser;
use colored::*;

use crate::modules::{
    colors::printcolors, cpu::getcpu, de::getde, gpu::getgpu, host::gethost, init::getinit,
    kernel::getkernel, memory::getmem, os::getos, pkgs::getpkg, shell::getshell,
    systemage::getsystemage, terminal::getterminalname, uptime::getuptime, userhost::getuserhost,
    wm::getwm,
};

const VERSION: &str = clap::crate_version!();
const ASCII: [&str; 7] = [
    "                         ",
    " ────█▀█▄▄▄▄─────██▄     ",
    " ────█▀▄▄▄▄█─────█▀▀█    ",
    " ─▄▄▄█─────█──▄▄▄█       ",
    " ██▀▄█─▄██▀█─███▀█       ",
    " ─▀▀▀──▀█▄█▀─▀█▄█▀       ",
    "                         ",
];


unsafe fn fetch() {
    getuserhost();
    println!("");
    getos();
    gethost();
    getkernel();
    getshell();
    getuptime();
    getpkg();
    println!("");
    getde();
    getwm();
    getterminalname();
    getcpu();
    getgpu();
    getmem();
    getinit();
    #[cfg(feature = "with-selinux")]
    {
        use crate::modules::selinux::getselinux;
        getselinux();
    }
    let _ = getsystemage();
    println!("");
    printcolors();
    println!("");
}

fn main() {
    let args = cli::Cli::parse();

    if args.version == true {
        println!("{}: v{}", "Nexusfetch".bold().blue(), VERSION);
        return;
    }

    unsafe {
        fetch()
    }

    /*
    match args.show {
        "user" => {
            getuserhost();
            return;
        }
        "os" => {
            getos();
            return;
        }
        "host" => {
            gethost();
            return;
        }
        "kernel" => {
            getkernel();
            return;
        }
        "shell" => {
            getshell();
            return;
        }
        "uptime" => {
            getuptime();
            return;
        }
        "pkgs" => {
            getpkg();
            println!("");
            return;
        }
        "de" => {
            getde();
            return;
        }
        "wm" => {
            getwm();
            return;
        }
        "terminalname" => {
            getterminalname();
            return;
        }
        "cpu" => {
            getcpu();
            return;
        }
        "gpu" => {
            getgpu();
            return;
        }
        "mem" => {
            getmem();
            return;
        }
        "selinux" => {
            getselinux();
            return;
        }

        _ => {
            println!("{}: Invalid module.", "ERROR".bold().red());
            return;
        }
    }*/
}
