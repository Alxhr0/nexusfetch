pub mod colors;
pub mod cpu;
pub mod de;
pub mod gpu;
pub mod host;
pub mod init;
pub mod kernel;
pub mod memory;
pub mod os;
pub mod pkgs;
pub mod shell;
pub mod systemage;
pub mod terminal;
pub mod uptime;
pub mod userhost;
pub mod wm;

// Only enable if feature with-selinux is enabled.
#[cfg(feature = "with-selinux")]
pub mod selinux;
